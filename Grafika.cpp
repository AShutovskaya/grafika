﻿#include <SFML/Graphics.hpp>
#include<thread>
#include<chrono>
using namespace std::chrono_literals;
int main()
{
	//Создание окна с определенным размером и надписью
    sf::RenderWindow window(sf::VideoMode(900, 600), L" Первая программа!");
	//круг
    sf::CircleShape shape(100.f);
	shape.setOrigin(50,100);
    shape.setFillColor(sf::Color::Green);
	int shape_x =750, shape_y = 100;
	shape.setPosition(shape_x, shape_y);
	//прям
	sf::RectangleShape shape2(sf::Vector2f( 200, 200));
	shape2.setFillColor(sf::Color::Red);
	shape2.setOrigin(50, 100);
	int shape2_m=750, shape2_n=500;
	shape2.setPosition(shape2_m, shape2_n);
	
	sf::CircleShape shape3(90.f, 9);
	shape3.setFillColor(sf::Color::White);
	int shape3_k = 778, shape3_p = 710;
	shape3.setPosition(shape3_k, shape3_p);
	shape3.setOrigin(60, 500);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		
	
		shape_x--;
		if (shape_x<=50)
		{
			shape_x=50;
		
		}
		shape.setPosition(shape_x, shape_y);
		
		shape2_m--;
		if (shape2_m <=50)
		{
			shape2_m = 50;

		}
		shape2.setPosition(shape2_m, shape2_n);
		shape3_k--;
		if (shape3_k <= 58)
			shape3_k = 58;
		shape3.setPosition(shape3_k, shape3_p);

		

		
        window.clear();
        window.draw(shape);
		window.draw(shape2);
		window.draw(shape3); 

		window.display();
		std::this_thread::sleep_for(10ms);
		
    }

    return 0;
}